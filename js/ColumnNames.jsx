import React from 'react';
import _ from 'underscore';
import {COLUMNS} from './config.js';

class ColumnNames extends React.Component {
    render() {

        return (
            <tr className = {'categories'}>
                {COLUMNS.map((column, i) => <ColumnName key={i} name={column.name} />)}
                <th>Λειτουργίες</th>
            </tr>
        );
    }
}

class ColumnName extends React.Component {
    render() {
        return (
            <th>{this.props.name}</th>
         )
    }
}

export default ColumnNames;