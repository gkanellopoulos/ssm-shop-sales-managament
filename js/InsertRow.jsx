import React from 'react';
import Input from './Input.jsx';
import _ from 'underscore';
import StatusMessage from './StatusMessage.jsx'
import {
  COLUMNS,
  validPriceMultiplierRegex
} from './config.js';

class InsertRow extends React.Component {
    constructor(props) {
        super(props);

        this.inputs = {};
        this.state = {
            formSubmitted: false
        };

        this.convertDateToDateTime = this.convertDateToDateTime.bind(this);
        this.clearInput = this.clearInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.attachToForm = this.attachToForm.bind(this);
    }

    handleSubmit(event) {
        let row = {},
            currentTime = new Date();

        _.each(COLUMNS, function(columnObj) {
            let soldPrice = parseInt(this.inputs['soldPrice'].state.value, 10);
            let supplierPrice = parseInt(this.inputs['supplierPrice'].state.value, 10);

            if (columnObj.fieldName === 'dateTime') {
                currentTime = this.convertDateToDateTime(currentTime);

                row[columnObj.fieldName] = currentTime;
            } else if (columnObj.fieldName === 'profit') {
                row[columnObj.fieldName] = soldPrice -  supplierPrice;

                // if a receipt is available subtract VAT from profit.
                if (/Ναί|Ναι/.test(this.inputs['receipt'].state.value)) {
                    row[columnObj.fieldName] = (row[columnObj.fieldName]) * 0.76;
                }
            } else if (columnObj.fieldName === 'priceMultiplier') {
                if (validPriceMultiplierRegex.test(row['product'])) {
                  if (supplierPrice === 0) {
                    row[columnObj.fieldName] = soldPrice.toFixed(2);
                  } else {
                    row[columnObj.fieldName] = (soldPrice / supplierPrice).toFixed(2);
                  }
                } else {
                  row[columnObj.fieldName] = 0
                }
            } else if (!_.isUndefined(this.inputs[columnObj.fieldName])) {
                row[columnObj.fieldName] = this.inputs[columnObj.fieldName].state.value;
            }
        }.bind(this));

        $.ajax({
            url: "http://localhost:7777/addPurchase",
            type: 'post', // performing a POST request
            data:  row,
            dataType: 'json',
            statusCode: {
                200:function () {
                    this.clearInput();
                    this.setState({submitStatusCode: 1, formSubmitted: true});
                    this.props.pushRow(row);

                    setTimeout(function() {
                        this.setState({formSubmitted: false});
                    }.bind(this), 5000);
                }.bind(this),
                0 : function () {
                    this.setState({submitStatusCode: 0, formSubmitted: true});

                    setTimeout(function() {
                        this.setState({formSubmitted: false});
                    }.bind(this), 5000);
                }.bind(this)
            }
        });

        event.preventDefault();
    }

    convertDateToDateTime(date) {
        return date.getUTCFullYear() + '-' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
            ('00' + date.getUTCDate()).slice(-2) + ' ' +
            ('00' + date.getUTCHours()).slice(-2) + ':' +
            ('00' + date.getUTCMinutes()).slice(-2) + ':' +
            ('00' + date.getUTCSeconds()).slice(-2);
    }

    clearInput() {
        _.each(this.inputs, function(input) {
            if (input.props.type === 'checkbox') {
                input.setState({value: 'Όχι'})
            } else {
                input.setState({value: ''})
            }
        });
    }

    attachToForm(component) {
        this.inputs[component.props.name] = component;
    }

    render() {

        return (
            <div className="entity-container">
                <div className="entity-title">KATAXΩΡΗΣΗ ΝΕΑΣ ΠΩΛΗΣΗΣ</div>
                <form className="form-horizontal" onSubmit={this.handleSubmit}>

                    {
                        COLUMNS.map((column, i) =>
                           column.visibleOnSubmit ?
                                    <Input key={i}
                                           showLabel={true}
                                           displayName={column.name}
                                           name={column.fieldName}
                                           type={column.type}
                                           extraOptions={column.extraOptions}
                                           placeholder={column.name  + '...'}
                                           attachToForm={this.attachToForm}
                                           formGroupClassName={'col-sm-12'}
                                     />
                               : ''
                        )
                    }

                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button className="btn btn-success" type="submit">Καταχώρηση</button>
                            <button type="button" onClick={this.clearInput} className="btn btn-link">Καθαρισμός</button>
                        </div>
                    </div>
                </form>

                {
                    this.state.formSubmitted ?
                    <StatusMessage className={this.state.submitStatusCode ? 'alert-success' : 'alert-warning'}
                                   message= {this.state.submitStatusCode ? 'Η καταχώρηση ολοκληρώθηκε με επιτυχία' : 'Η καταχώρηση απέτυχε!'}
                    /> : ''
                }
            </div>
        );
    }
}

export default InsertRow;
