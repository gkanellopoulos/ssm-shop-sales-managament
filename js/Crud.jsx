import React from 'react';
import ReactDOM from 'react-dom';
import Row from './Row.jsx';
import InsertRow from './InsertRow.jsx';
import ColumnNames from './ColumnNames.jsx';
import _ from 'underscore';
import SearchRows from './SearchRows.jsx';
import InsertExpense from './InsertExpense.jsx';
import ExpenseRow from './ExpenseRow.jsx';
import Login from './Login.jsx';
import { validPriceMultiplierRegex } from './config.js';

class Crud extends React.Component {
    constructor() {
        super();

        this.state = {
            rows: [],
            expenseRows: [],
            cashCollectedToday: 0,
            cardMoneyCollectedToday: 0,
            expensesToday: 0,
            sumPriceMultiplier: 0,
            isLoggedIn: 0
        };

        this.pushExpenseRow = this.pushExpenseRow.bind(this);
        this.pushRow = this.pushRow.bind(this);
        this.renderTodaysExpenses = this.renderTodaysExpenses.bind(this);
        this.renderTodaysPurchases = this.renderTodaysPurchases.bind(this);
        this.logIn = this.logIn.bind(this);
    }

    pushRow(newPurchase) {
        let newPurchasesArray = this.state.rows,
            cashCollectedToday = this.state.cashCollectedToday,
            cardMoneyCollectedToday = this.state.cardMoneyCollectedToday,
            sumPriceMultiplier = this.state.sumPriceMultiplier;

        newPurchasesArray.push(new Row(newPurchase));

        //if payment was not with credit card
        if (!/Ναί|Ναι/.test(newPurchase['creditCard'])) {
            cashCollectedToday += parseInt(newPurchase['moneyCollected'], 10);
        } else {
            cardMoneyCollectedToday += parseInt(newPurchase['moneyCollected'], 10);
        }

        if (validPriceMultiplierRegex.test(newPurchase['product'])) {
            sumPriceMultiplier += parseInt(newPurchase['priceMultiplier'], 10);
        }

        this.setState({
            rows: newPurchasesArray,
            cashCollectedToday : cashCollectedToday,
            cardMoneyCollectedToday: cardMoneyCollectedToday,
            sumPriceMultiplier: sumPriceMultiplier
        });
    }

    pushExpenseRow(newExpenseRow) {
        let newExpensesArray = this.state.expenseRows,
            expensesToday = this.state.expensesToday;

        newExpensesArray.push(new ExpenseRow(newExpenseRow));

        expensesToday += parseInt(newExpenseRow['price'], 10);

        this.setState({
            expenseRows: newExpensesArray,
            expensesToday: expensesToday,
        });
    }

    componentWillMount() {
        this.renderTodaysPurchases();
        this.renderTodaysExpenses();
    }

    renderTodaysPurchases() {
        let supplierPriceCollectedToday = 0,
            initialPriceCollectedToday = 0,
            moneyCollectedToday = 0,
            realMoneyCollectedToday = 0,
            cashCollectedToday = 0,
            cardMoneyCollectedToday = 0,
            sumPriceMultiplier = 0;

        this.setState({
            rows: [],
        });

        //first call to retrieve all purchases for today
        $.ajax({
            url: "http://127.0.0.1:7777/purchases",
            context: document.body
        }).done(function(rows) {
            _.each(JSON.parse(rows), function(dbRow) {
                this.pushRow(dbRow);

                supplierPriceCollectedToday += parseInt(dbRow['supplierPrice'], 10);
                initialPriceCollectedToday += parseInt(dbRow['sellPrice'], 10);
                moneyCollectedToday += parseInt(dbRow['soldPrice'], 10);
                realMoneyCollectedToday += parseInt(dbRow['moneyCollected'], 10);

                //if payment was not with credit card
                if (!/Ναί|Ναι/.test(dbRow['creditCard'])) {
                    cashCollectedToday += parseInt(dbRow['moneyCollected'], 10);
                } else {
                    cardMoneyCollectedToday += parseInt(dbRow['moneyCollected'], 10);
                }

                if (validPriceMultiplierRegex.test(dbRow['product'])) {
                    sumPriceMultiplier +=  parseInt(dbRow['priceMultiplier'], 10);
                }
            }.bind(this));

            this.setState({
                supplierPriceCollectedToday: supplierPriceCollectedToday,
                initialPriceCollectedToday: initialPriceCollectedToday,
                moneyCollectedToday: moneyCollectedToday,
                realMoneyCollectedToday: realMoneyCollectedToday,
                cashCollectedToday: cashCollectedToday,
                cardMoneyCollectedToday: cardMoneyCollectedToday,
                sumPriceMultiplier: sumPriceMultiplier
            });
            console.log(rows);
        }.bind(this));
    }

    renderTodaysExpenses() {
        let expensesToday = 0;

        this.setState({
            expenseRows: [],
        });

        //first call to retrieve all expenses for today
        $.ajax({
            url: "http://127.0.0.1:7777/expenses",
            context: document.body
        }).done(function(rows) {
            _.each(JSON.parse(rows), function(dbRow) {
                this.pushExpenseRow(dbRow);

                expensesToday += parseInt(dbRow['price'], 10)
            }.bind(this));

            this.setState({
                expensesToday: expensesToday,
            });
            console.log(rows);
        }.bind(this));
    }

    logIn() {
      this.setState({
          isLoggedIn: 1
      });
    }

    render() {
        let currentDate = new Date();

        if (!this.state.isLoggedIn) {
          return (
             <Login key="login" login={this.logIn} />
          )
        }

        return (
            <div>
                <div className='title text-center'>Διαχείριση Πωλήσεων</div>

                <img src="images/prooptiki_logo.png"/>

                <div className="entity-container">
                    <div className="entity-title">OΙ ΣΗΜΕΡΙΝΕΣ ΠΩΛΗΣΕΙΣ, {currentDate.toLocaleDateString()}</div>
                    <table className="table table-striped table-hover todays-purchases-table">
                        <thead>
                             <ColumnNames key="column-names" />
                        </thead>

                        <tbody>
                             {this.state.rows.map((row, i) => <Row key = {i}
                                                                   data = {row}
                                                                   renderTodaysPurchases={this.renderTodaysPurchases} />)}
                        </tbody>
                    </table>

                    <div className="entity-title">ΤΑ ΣΗΜΕΡΙΝΑ ΕΞΟΔΑ, {currentDate.toLocaleDateString()}</div>
                    <table className="table table-striped table-hover">
                        <thead>
                           <tr>
                               <th>Πρός</th>
                               <th>Ποσό</th>
                               <th>Λόγος</th>
                               <th>Ημερομηνία</th>
                               <th>Λειτουργίες</th>
                           </tr>
                        </thead>

                        <tbody>
                            {this.state.expenseRows.map((expenseRow, i) => <ExpenseRow renderTodaysExpenses={this.renderTodaysExpenses}
                                                                                       key = {i}
                                                                                       data = {expenseRow} />)}
                        </tbody>
                    </table>

                    <div className="entity-title">ΣΥΝΟΛΟ ΕΙΣΠΡΑΞΕΩΝ/ΕΞΟΔΩΝ ΗΜΕΡΑΣ, {currentDate.toLocaleDateString()}</div>

                    <table className="table table-striped table-hover">
                        <thead>
                           <tr>
                               <th>Αθρ. Τιμής Προμ.</th>
                               <th>Αθρ. Αρχικής Τιμής</th>
                               <th>Αθρ. Τελικής Τιμής</th>
                               <th>Αθρ. Είσπραξης</th>
                               <th>Mετρητά</th>
                               <th>Κάρτες</th>
                               <th>Μέσος Όρος Επί</th>
                               <th>Έξοδα</th>
                           </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <th><b>{this.state.supplierPriceCollectedToday} €</b></th>
                                <th><b>{this.state.initialPriceCollectedToday} €</b></th>
                                <th><b>{this.state.moneyCollectedToday} €</b></th>
                                <th><b>{this.state.realMoneyCollectedToday} €</b></th>
                                <th><b>{this.state.cashCollectedToday} €</b></th>
                                <th><b>{this.state.cardMoneyCollectedToday} €</b></th>
                                <th><b>{this.state.rows.length > 0 ?
                                        (this.state.sumPriceMultiplier / this.state.rows.length).toFixed(2) : 0}</b></th>
                                <th><b>{this.state.expensesToday} €</b></th>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div className="new-purchase-form-fields">
                    <InsertRow pushRow = {this.pushRow}/>
                </div>

                <div className="new-purchase-form-fields">
                    <InsertExpense pushExpenseRow = {this.pushExpenseRow} />
                </div>

                <SearchRows renderTodaysPurchases={this.renderTodaysPurchases}
                            renderTodaysExpenses={this.renderTodaysExpenses} />
            </div>
        );
    }
}

export default Crud;
