import React from 'react';
import Input from './Input.jsx';
import _ from 'underscore';
import {
    COLUMNS,
    validPriceMultiplierRegex
  }  from './config.js';
import Row from './Row.jsx';
import ExpenseRow from './ExpenseRow.jsx';
import RowPerDate from './RowPerDate.jsx';
import ExpenseRowPerDate from './ExpenseRowPerDate.jsx';
import ColumnNames from './ColumnNames.jsx';

class SearchRows extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputs: {},
            rows: [],
            rowsPerDate: [],
            expenseRows: [],
            expenseRowsPerDate: [],
            allSalesPagination: {
              currentPage: 1
            },
            expensesPagination: {
              currentPage: 1
            },
            rowsPerPage: 15
        };

        this.isInputValueValid = this.isInputValueValid.bind(this);
        this.isDateRangeValid = this.isDateRangeValid.bind(this);
        this.setResultsPerDate = this.setResultsPerDate.bind(this);
        this.setResults = this.setResults.bind(this);
        this.setExpenseResults = this.setExpenseResults.bind(this);
        this.showResults = this.showResults.bind(this);
        this.clearInput = this.clearInput.bind(this);
        this.attachToForm = this.attachToForm.bind(this);
        this.renderSalesPerDateChart = this.renderSalesPerDateChart.bind(this);
        this.handleAllSalesPaginationClick = this.handleAllSalesPaginationClick.bind(this);
        this.handleExpensesPaginationClick = this.handleExpensesPaginationClick.bind(this);
    }

    handleAllSalesPaginationClick(event) {
      this.setState({
        allSalesPagination: {
          currentPage: Number(event.target.id)
        }
      });
    }

    handleExpensesPaginationClick(event) {
      this.setState({
        expensesPagination: {
          currentPage: Number(event.target.id)
        }
      });
    }

    setResults(rows) {
        let newResultsArray = [];

        _.each(rows, function(dbRow) {
            newResultsArray.push(new Row(dbRow));
        }.bind(this));

        this.setState({rows: newResultsArray});
    }

    setExpenseResults(expenseRows) {
        let newExpensesArray = [];

        _.each(expenseRows, function(dbRow) {
            newExpensesArray.push(new ExpenseRow(dbRow));
        }.bind(this));

        this.setState({
            expenseRows: newExpensesArray,
        });
    }

    clearInput() {
        _.each(this.state.inputs, function(input) {
            input.setState({value: ''})
        });
    }

    attachToForm(component) {
        this.state.inputs[component.props.name] = component;
    }

    isDateRangeValid(dateFrom, dateTo) {
        return (
          !_.isUndefined(dateFrom) &&
          !_.isUndefined(dateTo) &&
          dateFrom !== '' &&
          dateTo !== ''
        );
    }

    isInputValueValid(inputValue, inputName) {
        return (
          inputValue !== '' &&
          !_.isUndefined(inputValue) &&
          inputValue !== null &&
          !/receipt|creditCard/.test(inputName)
        );
    }

    showResults() {
        let searchFields = {},
            validSearchFields = 0;

        setTimeout(function() {

            _.each(this.state.inputs, function(inputObj, inputName) {
                // Ignore minMoneyCollected input field as it is being used only on front end by client.
                if (inputName !== 'minMoneyToBeCollected') {
                   searchFields[inputName] = inputObj.state.value;

                   if (this.isInputValueValid(searchFields[inputName], inputName)) {
                       validSearchFields++;
                   }
                }
            }.bind(this));

            // Show each sale for all dates if they are set other wise for any other field/s prefix/es.
            if ((this.isDateRangeValid(searchFields['dateTimeFrom'], searchFields['dateTimeTo'])) ||
                (searchFields['dateTimeFrom'] === '' && searchFields['dateTimeTo'] === '') && validSearchFields > 0) {
                $.ajax({
                    url: "http://127.0.0.1:7777/searchResults",
                    type: 'post', // performing a POST request
                    data: searchFields,
                    dataType: 'json',
                }).done(function (results) {
                    this.setResults(results.sales);
                    this.setExpenseResults(results.expenses);
                }.bind(this));
            }

            // Show total money collected per date for all dates.
            $('#sales-per-date-chart').remove();
            if (this.isDateRangeValid(searchFields['dateTimeFrom'], searchFields['dateTimeTo'])) {
                $('.chart-container').append('<canvas id="sales-per-date-chart"></canvas>');

                $.ajax({
                    url: "http://127.0.0.1:7777/searchResultsPerDay",
                    type: 'post', // performing a POST request
                    data: searchFields,
                    dataType: 'json',
                }).done(function (results) {
                     this.setResultsPerDate(results);
                }.bind(this));
            }
        }.bind(this), 250);
    }

    setResultsPerDate(results) {
       let totalProfit = 0,
           dayProfit,
           profitPercentage,
           dates = [],
           sumMoneyPerDate = [],
           salesPerDate = [],
           expenseRowsPerDate = [],
           minMoneyToBeCollected = !_.isUndefined(this.state.inputs['minMoneyToBeCollected']) && /\d/.test(this.state.inputs['minMoneyToBeCollected'].state.value) ?
               parseInt(this.state.inputs['minMoneyToBeCollected'].state.value, 10) : '',
           moneyToBeCollected = minMoneyToBeCollected,
           totalSumsPerDate = {},
           sumDaySupplierPrice = 0,
           sumDayInitialPrice = 0,
           sumDayMoneyCollected = 0,
           sumDayRealMoneyCollected = 0,
           sumAveragePriceMultiplier = 0,
           totalExpensesOfDates = 0,
           totalSumExpensesOfDates = {};

       _.each(results.sales, function (rowObj) {
         dayProfit =  minMoneyToBeCollected !== '' ? rowObj.sumDayRealMoneyCollected - minMoneyToBeCollected : '';

         if (moneyToBeCollected !== '') {
             if (totalProfit > 0) {
               totalProfit += rowObj.sumDayRealMoneyCollected - moneyToBeCollected
             } else {
               totalProfit = rowObj.sumDayRealMoneyCollected - moneyToBeCollected
             }
           } else {
             totalProfit = '';
           }

           // Set data for sales per date chart.
           dates.push(moment(rowObj.dateTime));
           sumMoneyPerDate.push(rowObj.sumDayRealMoneyCollected);
           salesPerDate.push(new RowPerDate(
               rowObj.sumDaySupplierPrice,
               rowObj.sumDayInitialPrice,
               rowObj.sumDayMoneyCollected,
               rowObj.sumDayRealMoneyCollected,
               dayProfit,
               totalProfit,
               rowObj.sumPriceMultiplier / rowObj.totalRows,
               rowObj.dateTime));

           //if (moneyToBeCollected !== rowObj.sumDayRealMoneyCollected) {
               if (moneyToBeCollected > rowObj.sumDayRealMoneyCollected) {
                   moneyToBeCollected += minMoneyToBeCollected - rowObj.sumDayRealMoneyCollected;
               } else {
                   moneyToBeCollected = minMoneyToBeCollected;
               }
          // }

           sumDaySupplierPrice += rowObj.sumDaySupplierPrice;
           sumDayInitialPrice += rowObj.sumDayInitialPrice;
           sumDayMoneyCollected += rowObj.sumDayMoneyCollected;
           sumDayRealMoneyCollected += rowObj.sumDayRealMoneyCollected;
           sumAveragePriceMultiplier += rowObj.sumPriceMultiplier / rowObj.totalRows;
       }.bind(this));

       // Last parameter (date) is undefined in order to display "-".
       totalSumsPerDate = new RowPerDate(
           sumDaySupplierPrice,
           sumDayInitialPrice,
           sumDayMoneyCollected,
           sumDayRealMoneyCollected,
           '',
           totalProfit,
           sumAveragePriceMultiplier / salesPerDate.length
       );

       _.each(results.expenses, function (rowObj) {
           expenseRowsPerDate.push(new ExpenseRowPerDate(rowObj.price, rowObj.dateTime));
           totalExpensesOfDates += rowObj.price;
       }.bind(this));

       // Second parameter (date) is undefined in order to display "-".
       totalSumExpensesOfDates = new ExpenseRowPerDate(totalExpensesOfDates)

       this.setState({
           rowsPerDate: salesPerDate,
           expenseRowsPerDate: expenseRowsPerDate,
           totalSumsPerDate: totalSumsPerDate,
           totalSumExpensesOfDates: totalSumExpensesOfDates
       });

       this.renderSalesPerDateChart(sumMoneyPerDate, dates);
    }

    renderSalesPerDateChart(moneyCollected, dates) {
        let ctx,
            salesPerDateChart;

            ctx = document.getElementById('sales-per-date-chart').getContext('2d');
            salesPerDateChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dates,
                    datasets: [{
                        label: "Ποσό Είσπραξης €",
                        data: moneyCollected,
                    }]
                },
                options: {
                    scales: {
                        xAxes: [{
                            type: 'time',
                            time: {
                                displayFormats: {
                                    'millisecond': 'MMM DD',
                                    'second': 'MMM DD',
                                    'minute': 'MMM DD',
                                    'hour': 'MMM DD',
                                    'day': 'MMM DD',
                                    'week': 'MMM DD',
                                    'month': 'MMM DD',
                                    'quarter': 'MMM DD',
                                    'year': 'MMM DD',
                                }
                            }
                        }],
                    },
                }
            });
    }

    render() {
        const { rows, expenseRows } = this.state;
        const { state } = this;

        //Pagination Common starts here.
        const rowsPerPage = state.rowsPerPage;
        //Pagination Common ends here.

        // Pagination of all sales starts here.
        // Logic for displaying all sales rows.
        const currentPageOfAllSales = state.allSalesPagination.currentPage;
        const indexOfLastRow = currentPageOfAllSales * rowsPerPage;
        const indexOfFirstRow = indexOfLastRow - rowsPerPage;
        const currentSalesRows = rows.slice(indexOfFirstRow, indexOfLastRow);

        // Logic for displaying page numbers of all sales.
        const pageNumbersOfAllSales = [];
        for (let i = 1; i <= Math.ceil(rows.length / rowsPerPage); i++) {
          pageNumbersOfAllSales.push(i);
        }

        const renderPageNumbersOfAllSales = pageNumbersOfAllSales.map(pageNumber => {
          return (
            <li className={currentPageOfAllSales == pageNumber ? 'active' : ''}
                key={pageNumber} id={pageNumber} onClick={this.handleAllSalesPaginationClick}>
              <span id={pageNumber}>{pageNumber}</span>
            </li>
          );
        });

        // Pagination of all sales ends here.

        // Pagination of all expenses starts here.
        // Logic for displaying expenses rows.
        const currentPageOfExpenses = state.expensesPagination.currentPage;
        const indexOfLastExpense = currentPageOfExpenses * rowsPerPage;
        const indexOfFirstExpense = indexOfLastExpense - rowsPerPage;
        const currentExpenseRows = expenseRows.slice(indexOfFirstExpense, indexOfLastExpense);

        // Logic for displaying page numbers of expenses.
        const pageNumbersOfExpenses = [];
        for (let i = 1; i <= Math.ceil(expenseRows.length / rowsPerPage); i++) {
          pageNumbersOfExpenses.push(i);
        }

        const renderPageNumbersOfExpenses= pageNumbersOfExpenses.map(pageNumber => {
          return (
            <li className={currentPageOfExpenses == pageNumber ? 'active' : ''}
                key={pageNumber} id={pageNumber} onClick={this.handleExpensesPaginationClick}>
              <span id={pageNumber}>{pageNumber}</span>
            </li>
          );
        });

        // Pagination of all expenses ends here.

        return (
            <div className="search-container entity-container">
                    <div className="entity-title">ΣΥΝΘΕΤΗ ΑΝΑΖΗΤΗΣΗ ΠΩΛΗΣΕΩΝ & EΞΟΔΩΝ </div><br/>

                    {
                        !_.isUndefined(this.state.rows) ?
                            <div className="entity-title"> ΟΛΕΣ ΟΙ ΠΩΛΗΣΕΙΣ / ΑΡ. ΠΩΛΗΣΕΩΝ: <b>{this.state.rows.length}</b> </div> : ''
                    }

                    <table className="table table-striped table-hover">
                        <thead>
                           <ColumnNames key="column-names" />
                        </thead>

                        <tbody>
                            <tr>
                                {
                                    COLUMNS.map((column, i) =>
                                           column.fieldName !== 'dateTime' && column.visibleOnSearch ?
                                            <td>
                                            <Input key={i}
                                                   showLabel={false}
                                                   name={column.fieldName}
                                                   type={column.type}
                                                   extraOptions={column.extraOptions}
                                                   placeholder={column.name  + '...'}
                                                   attachToForm={this.attachToForm}
                                                   updateParentCallback={this.showResults}
                                            />
                                            </td> :

                                           <td className="date-from-to">
                                               <Input key={'dateTimeFrom'}
                                                      showLabel={false}
                                                      name={'dateTimeFrom'}
                                                      type={'date'}
                                                      attachToForm={this.attachToForm}
                                                      updateParentCallback={this.showResults}
                                                      formGroupClassName={'col-sm-6 date-from'}
                                               />

                                               <Input key={'dateTimeTo'}
                                                      showLabel={false}
                                                      name={'dateTimeTo'}
                                                      type={'date'}
                                                      attachToForm={this.attachToForm}
                                                      updateParentCallback={this.showResults}
                                                      formGroupClassName={'col-sm-6 date-to'}
                                               />
                                           </td>
                                    )
                                }
                                <td></td>
                            </tr>

                            {currentSalesRows.map((row, i) =>
                                <Row key = {i}
                                     data = {row}
                                     showResults={this.showResults}
                                     renderTodaysPurchases={this.props.renderTodaysPurchases} />)}
                        </tbody>
                    </table>

                    <ul className="pagination">
                      {renderPageNumbersOfAllSales}
                    </ul>

                    <div className="entity-title">ΟΛΑ ΤΑ ΕΞΟΔΑ</div>
                    <table className="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Πρός</th>
                            <th>Ποσό</th>
                            <th>Λόγος</th>
                            <th>Ημερομηνία</th>
                            <th>Λειτουργίες</th>
                        </tr>
                        </thead>

                        <tbody>
                        {currentExpenseRows.map((expenseRow, i) =>
                            <ExpenseRow showResults={this.showResults}
                                        renderTodaysExpenses={this.props.renderTodaysExpenses}
                                        key = {i}
                                        data = {expenseRow} />)}
                        </tbody>
                    </table>

                    <ul className="pagination">
                      {renderPageNumbersOfExpenses}
                    </ul>

                    <div className="entity-title">ΑΘΡΟΙΣΜΑ ΕΣΟΔΩΝ / ΕΞΟΔΩΝ ΑΝΑ ΗΜΕΡΑ</div>

                    <Input key={'minMoneyToBeCollected'}
                           showLabel={true}
                           name={'minMoneyToBeCollected'}
                           displayName={'Ελάχιστος Τζίρος Ημέρας'}
                           type={'number'}
                           attachToForm={this.attachToForm}
                           formGroupClassName={'col-sm-12'}
                           updateParentCallback={this.showResults}
                    />

                    <div className="entity-title">ΑΘΡΟΙΣΜΑ ΕΣΟΔΩΝ</div>

                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Αθρ. Τιμής Προμ.</th>
                                <th>Αθρ. Αρχικής Τιμής</th>
                                <th>Αθρ. Τελικής Τιμής Πώλησης</th>
                                <th>Αθρ. Είσπραξης</th>
                                <th>Κέρδος/Ζημιά Ημέρας</th>
                                <th>Συνολικό Κέρδος/Ζημιά</th>
                                <th>Μέσος Όρος Επί</th>
                                <th>Ημερομηνία</th>
                            </tr>
                        </thead>

                        <tbody id="sums-sales-per-date-table-body">
                            {this.state.rowsPerDate.map((rowPerDate, i) => <RowPerDate key = {i} data = {rowPerDate} />)}
                            <b>ΣΥΝΟΛΟ ΑΘΡΟΙΣΜΑΤΩΝ</b>
                            {
                              !_.isUndefined(this.state.totalSumsPerDate) ?
                                <RowPerDate key = 'total-sums-row' data = {this.state.totalSumsPerDate} /> :
                                ''
                            }
                        </tbody>
                    </table>

                    <div className="entity-title">ΑΘΡΟΙΣΜΑ ΕΞΟΔΩΝ</div>

                    <table className="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Έξοδα</th>
                            <th>Ημερομηνία</th>
                        </tr>
                        </thead>

                        <tbody id="sums-expenses-per-date-table-body">
                        {this.state.expenseRowsPerDate.map((rowPerDate, i) => <ExpenseRowPerDate key = {i} data = {rowPerDate} />)}
                        <b>ΣΥΝΟΛΟ ΕΞΟΔΩΝ</b>
                        {
                          !_.isUndefined(this.state.totalSumExpensesOfDates) ?
                            <ExpenseRowPerDate key = 'total-sums-expenses-row' data = {this.state.totalSumExpensesOfDates} /> :
                            ''
                        }
                        </tbody>
                    </table>

                    <div className="entity-title">ΔΙΑΓΡΑΜΜΑ ΕΙΣΠΡΑΞΕΩΝ ΑΝΑ ΗΜΕΡΑ</div>

                    <div className="chart-container">
                        <canvas id="sales-per-date-chart"></canvas>
                    </div>
            </div>
        );
    }
}

export default SearchRows;
