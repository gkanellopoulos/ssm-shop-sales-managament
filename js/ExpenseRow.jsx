import React from 'react';
import Input from './Input.jsx'
import _ from 'underscore';

class ExpenseRow extends React.Component {
    constructor(newExpenseRow) {
        let formattedDate;
        super();

        formattedDate = new Date(newExpenseRow.dateTime);

        this.id = newExpenseRow.id;
        this.date = formattedDate.toDateString();
        this.recipient = newExpenseRow.recipient;
        this.reason = newExpenseRow.reason;
        this.price = newExpenseRow.price;

        this.state = {
            isEditMode: false
        };

        this.inputs = {};
        this.attachToForm = this.attachToForm.bind(this);
        this.cancelEditMode = this.cancelEditMode.bind(this);
        this.editMode = this.editMode.bind(this);
        this.editRow = this.editRow.bind(this);
        this.deleteRow = this.deleteRow.bind(this);
    }

    deleteRow() {
        let row = {};

        row['id'] = this.props.data.id;

        $.ajax({
            url: "http://127.0.0.1:7777/deleteExpense",
            type: 'post', // performing a POST request
            data:  row,
            dataType: 'json',
            statusCode: {
                200:function () {
                    this.setState({isEditMode: false});

                    // Adding check cause if update from todays expenses we dont have the callback of child (showResults).
                    if (!_.isUndefined(this.props.showResults)) {
                        this.props.showResults();
                    }

                    this.props.renderTodaysExpenses();
                }.bind(this),
            }
        });
    }

    cancelEditMode() {
        this.setState({
            isEditMode: false
        });
    }

    editMode() {
        this.setState({
            isEditMode: true
        });
    }

    editRow() {
        let row = {};

        row['id'] = this.props.data.id;
        row['price'] = this.inputs['price'].state.value;
        row['recipient'] = this.inputs['recipient'].state.value;
        row['reason'] = this.inputs['reason'].state.value;
        row['dateTime'] = this.inputs['dateTime'].state.value;

        $.ajax({
            url: "http://127.0.0.1:7777/editExpense",
            type: 'post', // performing a POST request
            data:  row,
            dataType: 'json',
            statusCode: {
                200:function () {
                    this.setState({isEditMode: false});

                    // Adding check cause if update from todays expenses we dont have the callback of child (showResults).
                    if (!_.isUndefined(this.props.showResults)) {
                        this.props.showResults();
                    }

                    this.props.renderTodaysExpenses();
                }.bind(this),
            }
        });

    }

    attachToForm(component) {
        this.inputs[component.props.name] = component;
    }

    render() {
        return (

            this.state.isEditMode ?
                <tr>
                    <th>
                        <Input showLabel={false}
                               displayName=''
                               name='recipient'
                               type='text'
                               placeholder=''
                               attachToForm={this.attachToForm}
                               formGroupClassName={'col-sm-12'}
                               value={this.props.data.recipient}
                        />
                    </th>
                    <th>
                        <Input showLabel={false}
                               displayName=''
                               name='price'
                               type='number'
                               placeholder=''
                               attachToForm={this.attachToForm}
                               formGroupClassName={'col-sm-12'}
                               value={this.props.data.price}
                        />
                    </th>
                    <th>
                        <Input showLabel={false}
                               displayName=''
                               name='reason'
                               type='text'
                               placeholder=''
                               attachToForm={this.attachToForm}
                               formGroupClassName={'col-sm-12'}
                               value={this.props.data.reason}
                        />
                    </th>
                    <th>
                        <Input key={'dateTime'}
                               showLabel={false}
                               name={'dateTime'}
                               type={'date'}
                               attachToForm={this.attachToForm}
                               formGroupClassName={'col-sm-12'}
                               value={this.props.data.date}
                        />
                    </th>
                    <th className="expense-actions">
                      <button className="btn btn-success left" onClick={this.editRow}>
                        <span className="glyphicon glyphicon-ok"></span>
                      </button>
                      <button className="btn btn-default right" onClick={this.cancelEditMode}>
                        <span className="glyphicon glyphicon-remove"></span>
                      </button>
                    </th>
                </tr>
                :
                <tr>
                    <th>{this.props.data.recipient}</th>
                    <th>{this.props.data.price !== '' ? this.props.data.price + " €" : '-'}</th>
                    <th>{this.props.data.reason}</th>
                    <th>{this.props.data.date}</th>
                    <th className="expense-actions">
                      <button className="btn btn-primary left" onClick={this.editMode}>
                        <span className="glyphicon glyphicon-pencil"></span>
                      </button>
                      <button className="btn btn-danger right" onClick={this.deleteRow}>
                        <span className="glyphicon glyphicon-trash"></span>
                      </button>
                    </th>
                </tr>
        );
    }
}

export default ExpenseRow;
