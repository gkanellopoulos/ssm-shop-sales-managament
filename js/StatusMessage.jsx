import React from 'react';

class StatusMessage extends React.Component {
    render() {
        return (
            <div className={"alert " + this.props.className} >
                <strong>{this.props.message}</strong>
            </div>
        )
    }
}

export default StatusMessage;



