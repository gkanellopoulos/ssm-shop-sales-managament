import React from 'react';
import _ from 'underscore';

class Input extends React.Component {
    constructor(props) {
        super(props);

        this.getValue = this.getValue.bind(this);
        this.setValue = this.setValue.bind(this);
    }

    componentWillMount() {
        this.setState({
            value: !_.isUndefined(this.props.value) ? this.getValue(this.props) : this.getDefaultValue()
        });

        this.props.attachToForm(this); // Attaching the component to the form
    }

    // Whenever the input changes we update the value state
    // of this component
    setValue(event) {
        this.setState({
            value: this.getValue(event.currentTarget)
        });


        if(!_.isUndefined(this.props.updateParentCallback)) {
            this.props.updateParentCallback();
        }
    }

    /**
    * {target} Notice! target can be an event obj from client or normal row obj
    */
    getValue(target) {
        let retValue,
            dateHumanFormat;

        switch(this.props.type) {
            case 'checkbox':
                retValue = target.checked ? 'Ναι' : 'Όχι';
                break;
            case 'date':
            case 'dateTime':
                dateHumanFormat = new Date(target.value);

                // Date is in UTC format but we want the GMT one. Only way to to dat is by Date's get methods
                retValue = dateHumanFormat.getUTCFullYear() + '-' +
                           ("0" + (dateHumanFormat.getMonth() + 1)).slice(-2) + '-' +
                           ("0" + dateHumanFormat.getDate()).slice(-2);
                break;
            case 'number':
            case 'text':
            case 'select':
            default:
                retValue = target.value;

        }

        return retValue;
    }

    getDefaultValue() {
        let defaultValue;

        switch(this.props.type) {
            case 'checkbox':
                defaultValue = 'Όχι';
                break;
            case 'number':
            case 'text':
            default:
                defaultValue = '';

        }

        return defaultValue;
    }

    render() {
        return (
            <div className={"form-group " + this.props.formGroupClassName}>
                {this.props.showLabel ?
                    <label className="col-sm-2 control-label">{this.props.displayName}</label> : ''}

                <div className={this.props.showLabel ? "col-sm-10" : "col-sm-12"}>
                    {
                      this.props.type === 'select' ?
                        <select value={this.state.value}
                                onChange={this.setValue}
                                className="form-control">
                          {
                            this.props.extraOptions.map((option, i) =>
                              <option key={option+i} value={option}>{option}</option>
                            )
                          }
                        </select>
                      :
                      <input className='form-control'
                             type={this.props.type} placeholder={this.props.placeholder}
                             name={this.props.name} onChange={this.setValue}
                             value={this.state.value}
                      />
                   }
                </div>
            </div>
        );
    }
}

export default Input;
