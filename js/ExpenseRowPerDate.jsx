import React from 'react';
import _ from 'underscore'

class ExpenseRowPerDate extends React.Component {
    constructor(price, date) {
        let formattedDate;
        super();

        if (_.isUndefined(date)) {
          this.date = '-';
        } else {
          formattedDate = new Date(date);
          this.date = formattedDate.toDateString();
        }

        this.price = price;
    }

    render() {
        return (
            <tr>
                <th>{this.props.data.expenses !== '' ? this.props.data.price + " €" : '-'}</th>
                <th>{this.props.data.date}</th>
            </tr>
        );
    }
}



export default ExpenseRowPerDate;
