import React from 'react';
import ReactDOM from 'react-dom';
import Crud from './Crud.jsx';

ReactDOM.render(<Crud />, document.getElementById('crud'));
