import React from 'react';
import Input from './Input.jsx';
import _ from 'underscore';
import StatusMessage from './StatusMessage.jsx'

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.inputs = {};
        this.state = {
            formSubmitted: false
        };

        this.clearInput = this.clearInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.attachToForm = this.attachToForm.bind(this);
    }

    handleSubmit(event) {
        let row = {};

        row['username'] = this.inputs['username'].state.value;
        row['password'] = this.inputs['password'].state.value;

        $.ajax({
            url: "http://127.0.0.1:7777/login",
            type: 'post', // performing a POST request
            data:  row,
            dataType: 'json',
            statusCode: {
                200: function () {
                    this.clearInput();
                    this.setState({submitStatusCode: 1, formSubmitted: true});
                    this.props.login();
                }.bind(this),
                400 : function () {
                    this.setState({submitStatusCode: 0, formSubmitted: true});

                    setTimeout(function() {
                        this.setState({formSubmitted: false});
                    }.bind(this), 5000);
                }.bind(this)
            }
        });

        event.preventDefault();
    }

    clearInput() {
        _.each(this.inputs, function(input) {
            input.setState({value: ''})
        });
    }

    attachToForm(component) {
        this.inputs[component.props.name] = component;
    }

    render() {

        return (
            <div className="login entity-container">
                <div className="entity-title">ΕΙΣΟΔΟΣ</div>
                <form className="form-horizontal" onSubmit={this.handleSubmit}>

                    <Input showLabel={true}
                           displayName='Username: '
                           name='username'
                           type='text'
                           placeholder={''}
                           attachToForm={this.attachToForm}
                           formGroupClassName={'col-sm-12'}
                    />

                    <Input showLabel={true}
                           displayName='Password: '
                           name='password'
                           type='password'
                           placeholder={''}
                           attachToForm={this.attachToForm}
                           formGroupClassName={'col-sm-12'}
                    />

                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button className="btn btn-success" type="submit">Sign In</button>
                            <button type="button" onClick={this.clearInput} className="btn btn-link">Clear</button>
                        </div>
                    </div>
                </form>

                {
                    this.state.formSubmitted ?
                        <StatusMessage className={this.state.submitStatusCode ? 'alert-success' : 'alert-warning'}
                                       message= {this.state.submitStatusCode ? 'Login Successful' : 'Login Failed!'}
                        /> : ''
                }
            </div>
        );
    }
}

export default Login;
