import React from 'react';
import _ from 'underscore';

class RowPerDate extends React.Component {
    constructor(sumSupplierPrice, sumInitialPrice, moneyCollected, realMoneyCollected, dayProfit, totalProfit, averagePriceMultiplier, date) {
        let formattedDate;
        super();

        if (_.isUndefined(date)) {
          this.date = '-';
        } else {
          formattedDate = new Date(date);
          this.date = formattedDate.toDateString();
        }

        this.sumSupplierPrice = sumSupplierPrice;
        this.sumInitialPrice = sumInitialPrice;
        this.realMoneyCollected = realMoneyCollected;
        this.moneyCollected = moneyCollected;
        this.profit = dayProfit;
        this.totalProfit = totalProfit;
        this.averagePriceMultiplier = averagePriceMultiplier;

        if (this.profit !== '' || this.profit === 0) {
            if (this.profit > 0) {
                this.profitClass = 'green';
            } else {
                this.profitClass = 'red';
            }
        } else {
            this.profitClass = '';
        }

        if (this.totalProfit !== '' || this.totalProfit === 0) {
            if (this.totalProfit > 0) {
                this.totalProfitClass = 'green';
            } else {
                this.totalProfitClass = 'red';
            }
        } else {
            this.totalProfitClass = '';
        }
    }

    render() {
        return (
            <tr>
                <th>{this.props.data.sumSupplierPrice !== '' ? this.props.data.sumSupplierPrice + " €" : '-'}</th>
                <th>{this.props.data.sumInitialPrice !== '' ? this.props.data.sumInitialPrice + " €" : '-'}</th>
                <th>{this.props.data.moneyCollected !== '' ? this.props.data.moneyCollected + " €" : '-'}</th>
                <th>{this.props.data.realMoneyCollected !== '' ? this.props.data.realMoneyCollected + " €" : '-'}</th>
                <th className={this.props.data.profitClass}>
                    {this.props.data.profit !== '' ? this.props.data.profit + " €" : '-'}
                </th>
                <th className={this.props.data.totalProfitClass}>
                    {this.props.data.totalProfit !== '' ? this.props.data.totalProfit + " €" : '-'}
                </th>
                <th>{this.props.data.averagePriceMultiplier.toFixed(2)}</th>
                <th>{this.props.data.date}</th>
            </tr>
        );
    }
}



export default RowPerDate;
