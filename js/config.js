'use strict';

class Column {
    constructor(fieldName, name, type, extraTextOnDisplay = '',
                visibleOnSubmit = true, visibleOnSearch = true, extraOptions = {}) {
        this.fieldName = fieldName;
        this.name = name;
        this.type = type;
        this.extraTextOnDisplay = extraTextOnDisplay;
        this.visibleOnSubmit = visibleOnSubmit;
        this.visibleOnSearch = visibleOnSearch;
        this.extraOptions = extraOptions;
    }
}

export const COLUMNS = [
    new Column("orderId", "Κωδ. Παραγγελίας", "number"),
    new Column("clientName", "Ονομα Πελάτη", "text"),
    new Column("product", "Προιόν", "select",'', true, true, [
        '-- Επιλέξτε ένα προιόν --',
        'ΓΥΑΛΙΑ ΗΛΙΟΥ',
        'ΓΥΑΛΙΑ ΟΡΑΣΕΩΣ',
        'ΠΑΡΑΔΟΣΗ ΓΥΑΛΙΩΝ ΗΛΙΟΥ',
        'ΠΑΡΑΔΟΣΗ ΓΥΑΛΙΩΝ ΟΡΑΣΕΩΣ',
        'ΣΚΕΛΕΤΟΣ',
        'ΚΡΥΣΤΑΛΛΑ',
        'ΦΑΚΟΙ ΕΠΑΦΗΣ',
        'ΥΓΡΟ ΦΑΚΩΝ',
        'ΑΣΗΜΟΚΟΛΛΗΣΗ',
        'ΘΗΚΗ',
        'ΚΟΡΔΟΝΙ',
        'ΕΝΑΝΤΙ ΧΡΕΟΥΣ'
    ]),
    new Column("supplierPrice", "Τιμή Προμηθευτή", "number", "€"),
    new Column("sellPrice", "Αρχική Τιμή Πώλησης", "number", "€"),
    new Column("soldPrice", "Τελική Τιμή Πώλησης", "number", "€"),
    new Column("moneyCollected", "Ποσό Είσσπραξης", "number", "€"),
    new Column("receipt", "Απόδειξη", "checkbox"),
    new Column("creditCard", "Κάρτα", "checkbox"),
    new Column("profit", "Kέρδος", "number", "€", false),
    new Column("priceMultiplier", "Επί (x)", "number", '', false),
    new Column("dateTime", "Hμερομηνία", "date", '', false)
];

export const validPriceMultiplierRegex =
  /ΓΥΑΛΙΑ ΗΛΙΟΥ|ΓΥΑΛΙΑ ΟΡΑΣΕΩΣ|ΣΚΕΛΕΤΟΣ|ΚΡΥΣΤΑΛΛΑ/;
