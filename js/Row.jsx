import React from 'react';
import Input from './Input.jsx'
import _ from 'underscore';
import {
  COLUMNS,
  validPriceMultiplierRegex
} from './config.js';

class Row extends React.Component {
    constructor(purchaseObj) {
        let formattedDate;
        super();

        this.id = purchaseObj.id;

        _.each(COLUMNS, function(columnObj) {
            if (!_.isUndefined(purchaseObj[columnObj.fieldName])) {
                if (columnObj.type === 'date') {
                    formattedDate = new Date(purchaseObj[columnObj.fieldName]);
                    purchaseObj[columnObj.fieldName] = formattedDate.toDateString();
                }

                this[columnObj.fieldName] = purchaseObj[columnObj.fieldName];
            }
        }.bind(this));

        this.state = {
            isEditMode: false
        };

        this.inputs = {};
        this.attachToForm = this.attachToForm.bind(this);
        this.cancelEditMode = this.cancelEditMode.bind(this);
        this.editMode = this.editMode.bind(this);
        this.editRow = this.editRow.bind(this);
        this.deleteRow = this.deleteRow.bind(this);
    }

    deleteRow() {
        let row = {};

        row['id'] = this.props.data.id;

        $.ajax({
            url: "http://127.0.0.1:7777/deletePurchase",
            type: 'post', // performing a POST request
            data:  row,
            dataType: 'json',
            statusCode: {
                200:function () {
                    this.setState({isEditMode: false});

                    // Adding check cause if update from todays purchases we dont have the callback of child (showResults).
                    if (!_.isUndefined(this.props.showResults)) {
                        this.props.showResults();
                    }

                    this.props.renderTodaysPurchases();
                }.bind(this),
            }
        });
    }

    cancelEditMode() {
        this.setState({
            isEditMode: false
        });
    }

    editMode() {
        this.setState({
            isEditMode: true
        });
    }

    editRow() {
        let row = {},
            currentTime;

        row['id'] = this.props.data.id;

        _.each(COLUMNS, function(columnObj) {
            if (columnObj.fieldName === 'dateTime') {
                currentTime = this.inputs['dateTime'].state.value;

                row[columnObj.fieldName] = currentTime;
            } else if (columnObj.fieldName === 'profit') {
                row[columnObj.fieldName] = this.inputs['soldPrice'].state.value -  this.inputs['supplierPrice'].state.value;

                // if a receipt is available subtract VAT from profit.
                if (/Ναί|Ναι/.test(this.inputs['receipt'].state.value)) {
                    row[columnObj.fieldName] = (row[columnObj.fieldName]) * 0.76;
                }
            } else if (columnObj.fieldName === 'priceMultiplier') {
                if (validPriceMultiplierRegex.test(row['product'])) {
                  if (this.inputs['supplierPrice'].state.value === 0) {
                    row[columnObj.fieldName] = this.inputs['soldPrice'].state.value.toFixed(2);
                  } else {
                    row[columnObj.fieldName] = (this.inputs['soldPrice'].state.value / this.inputs['supplierPrice'].state.value).toFixed(2);
                  }
                } else {
                    row[columnObj.fieldName] = 0;
                }
            } else if (!_.isUndefined(this.inputs[columnObj.fieldName])) {
                row[columnObj.fieldName] = this.inputs[columnObj.fieldName].state.value;
            }
        }.bind(this));

        $.ajax({
            url: "http://127.0.0.1:7777/editPurchase",
            type: 'post', // performing a POST request
            data:  row,
            dataType: 'json',
            statusCode: {
                200: function () {
                    this.setState({isEditMode: false});

                    // Adding check cause if update from todays purchases we dont have the callback of child (showResults).
                    if (!_.isUndefined(this.props.showResults)) {
                        this.props.showResults();
                    }

                    this.props.renderTodaysPurchases();
                }.bind(this),
            }
        });

    }

    attachToForm(component) {
        this.inputs[component.props.name] = component;
    }

    render() {
        return (
            this.state.isEditMode ?
                <tr>
                    {COLUMNS.map((column, i) =>
                        <th>
                            {
                                !/priceMultiplier|profit/.test(column.fieldName) ?
                                    <Input showLabel={false}
                                           displayName=''
                                           name={column.fieldName}
                                           type={column.type}
                                          extraOptions={column.extraOptions}
                                           placeholder=''
                                           attachToForm={this.attachToForm}
                                           formGroupClassName={'col-sm-12'}
                                           value={this.props.data[column.fieldName]}/>
                                        : ''
                            }
                        </th>
                    )}
                    <th className="actions">
                        <button className="btn btn-success left" onClick={this.editRow}>
                          <span className="glyphicon glyphicon-ok"></span>
                        </button>
                        <button className="btn btn-default right" onClick={this.cancelEditMode}>
                          <span className="glyphicon glyphicon-remove"></span>
                        </button>
                    </th>
                </tr> :
                <tr>
                    {COLUMNS.map((column, i) =>
                        <Field key={i}
                        extraTextOnDisplay={!_.isUndefined(column.extraTextOnDisplay) ? column.extraTextOnDisplay : ''}
                        fieldValue={this.props.data[column.fieldName]} />
                    )}
                    <th className="actions">
                        <button className="btn btn-primary left" onClick={this.editMode}>
                          <span className="glyphicon glyphicon-pencil"></span>
                        </button>
                        <button className="btn btn-danger right" onClick={this.deleteRow}>
                          <span className="glyphicon glyphicon-trash"></span>
                        </button>
                    </th>
                </tr>
        );
    }
}

class Field extends React.Component {
    render() {
        return (
            <th>{!_.isUndefined(this.props.fieldValue) ? this.props.fieldValue + " " + this.props.extraTextOnDisplay : ''}</th>
        )
    }
}



export default Row;
