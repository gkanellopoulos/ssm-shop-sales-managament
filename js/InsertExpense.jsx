import React from 'react';
import Input from './Input.jsx';
import _ from 'underscore';
import StatusMessage from './StatusMessage.jsx'

class InsertExpense extends React.Component {
    constructor(props) {
        super(props);

        this.inputs = {};
        this.state = {
            formSubmitted: false
        };

        this.convertDateToDateTime = this.convertDateToDateTime.bind(this);
        this.clearInput = this.clearInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.attachToForm = this.attachToForm.bind(this);
    }

    handleSubmit(event) {
        let row = {},
            currentTime = new Date();

        row['price'] = this.inputs['price'].state.value;
        row['recipient'] = this.inputs['recipient'].state.value;
        row['reason'] = this.inputs['reason'].state.value;

        currentTime = this.convertDateToDateTime(currentTime);
        row['dateTime'] = currentTime;

        $.ajax({
            url: "http://127.0.0.1:7777/addExpense",
            type: 'post', // performing a POST request
            data:  row,
            dataType: 'json',
            statusCode: {
                200: function () {
                    this.clearInput();
                    this.setState({submitStatusCode: 1, formSubmitted: true});
                    this.props.pushExpenseRow(row);

                    setTimeout(function() {
                        this.setState({formSubmitted: false});
                    }.bind(this), 5000);
                }.bind(this),
                400 : function () {
                    this.setState({submitStatusCode: 0, formSubmitted: true});

                    setTimeout(function() {
                        this.setState({formSubmitted: false});
                    }.bind(this), 5000);
                }.bind(this)
            }
        });

        event.preventDefault();
    }

    convertDateToDateTime(date) {
        return date.getUTCFullYear() + '-' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
            ('00' + date.getUTCDate()).slice(-2) + ' ' +
            ('00' + date.getUTCHours()).slice(-2) + ':' +
            ('00' + date.getUTCMinutes()).slice(-2) + ':' +
            ('00' + date.getUTCSeconds()).slice(-2);
    }

    clearInput() {
        _.each(this.inputs, function(input) {
            input.setState({value: ''})
        });
    }

    attachToForm(component) {
        this.inputs[component.props.name] = component;
    }

    render() {

        return (
            <div className="entity-container">
                <div className="entity-title">KATAXΩΡΗΣΗ ΕΞΟΔΟΥ</div>
                <form className="form-horizontal" onSubmit={this.handleSubmit}>

                    <Input showLabel={true}
                           displayName='Προς: '
                           name='recipient'
                           type='text'
                           placeholder={'Προς...'}
                           attachToForm={this.attachToForm}
                           formGroupClassName={'col-sm-12'}
                    />

                    <Input showLabel={true}
                           displayName='Ποσό: '
                           name='price'
                           type='number'
                           placeholder={'Ποσό...'}
                           attachToForm={this.attachToForm}
                           formGroupClassName={'col-sm-12'}
                    />

                    <Input showLabel={true}
                           displayName='Λόγος: '
                           name='reason'
                           type='text'
                           placeholder={'Λόγος...'}
                           attachToForm={this.attachToForm}
                           formGroupClassName={'col-sm-12'}
                    />


                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button className="btn btn-success" type="submit">Καταχώρηση</button>
                            <button type="button" onClick={this.clearInput} className="btn btn-link">Καθαρισμός</button>
                        </div>
                    </div>
                </form>

                {
                    this.state.formSubmitted ?
                        <StatusMessage className={this.state.submitStatusCode ? 'alert-success' : 'alert-warning'}
                                       message= {this.state.submitStatusCode ? 'Η καταχώρηση ολοκληρώθηκε με επιτυχία' : 'Η καταχώρηση απέτυχε!'}
                        /> : ''
                }
            </div>
        );
    }
}

export default InsertExpense;
