- Digital Ocean:

ssh root@138.68.107.177

- Stop deamon server.js

forever stop 0

- Start deamon on server.js

forever start server.js

- Compile, publish, start server.js

npm run compile-publish-export-images-start
