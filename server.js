"use strict";

let express = require('express'),
    bodyParser =  require("body-parser"),
    _ = require('underscore'),
    app = express(),
    mysql = require('mysql'),
    connection;

require('shelljs/global');

app.use(express.static('public'));
app.use(express.static('public/js'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'shop-sales-management'
});

connection.connect(function(err){
    if(!err) {
        console.log("Database is connected ... \n\n");
    } else {
        console.log("Error connecting database ... \n\n" + err);
    }
});

app.all('/purchases', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/expenses', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/addPurchase', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/editPurchase', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/deletePurchase', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/addExpense', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/editExpense', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/deleteExpense', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/searchResults', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/searchResultsPerDay', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.all('/login', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.post('/login', function(req, res) {
    let post,
        query;
    console.log(req.body);

    post = req.body;

    query = connection.query('SELECT * FROM account WHERE username = ? AND password = ? ', [post.username, post.password], function(err, result) {
        console.log(query.sql);
        if (!err && result.length) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
            console.log('Error while performing Login Query.');
        }
    });
});

app.post('/addPurchase', function(req, res) {
    let post,
        query,
        mailContent;
     console.log(req.body);

     post = req.body;

     query = connection.query('INSERT INTO purchases SET ?', post, function(err, result) {
        if (!err) {
            mailContent = 'Kωδικός Παραγγελίας: ' + post.orderId +  ' Ονομα Πελάτη: ' + post.clientName +  ' Αγορά: ' + post.product + ' Τιμή Πώλησης: ' + post.soldPrice;

            console.log('Inserted purchase Row');
            console.log('Sending mail to gianniskane@gmail.com');
            console.log('echo "' + mailContent + '" | mailx -s  "Νεα καταχώρηση ' + post.orderId + '" gianniskane@gmail.com');
            exec('echo "' + mailContent + '" | mailx -s  "Νεα καταχώρηση ' + post.orderId + '" gianniskane@gmail.com');

            res.sendStatus(200);
        } else {
            res.sendStatus(400);
            console.log('Error while performing Inserting purchase Row Query.');
        }
         console.log(query.sql);
    });
});


app.post('/addExpense', function(req, res) {
    let post,
        query;
    console.log(req.body);

    post = req.body;

    query = connection.query('INSERT INTO expenses SET ?', post, function(err, result) {
        if (!err) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
            console.log('Error while performing Inserting Expense Query.');
        }
        console.log(query.sql);
    });
});

app.post('/editExpense', function(req, res) {
    let post,
        query;
    console.log(req.body);

    post = req.body;

    query = connection.query('UPDATE expenses SET ? WHERE id = ? ', [post, post.id], function(err, result) {
        if (!err) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
            console.log('Error while performing Inserting Expense Query.');
        }
        console.log(query.sql);
    });
});

app.post('/deleteExpense', function(req, res) {
    let post,
        query;
    console.log(req.body);

    post = req.body;

    query = connection.query('DELETE from expenses WHERE id = ? ', [post.id], function(err, result) {
        if (!err) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
            console.log('Error while performing Inserting Expense Query.');
        }
        console.log(query.sql);
    });
});

app.post('/deletePurchase', function(req, res) {
    let post,
        query;
    console.log(req.body);

    post = req.body;

    query = connection.query('DELETE from purchases WHERE id = ? ', [post.id], function(err, result) {
        if (!err) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
            console.log('Error while performing Inserting Expense Query.');
        }
        console.log(query.sql);
    });
});

app.post('/editPurchase', function(req, res) {
    let post,
        query;
    console.log(req.body);

    post = req.body;

    query = connection.query('UPDATE purchases SET ? WHERE id = ? ', [post, post.id], function(err, result) {
        if (!err) {
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
            console.log('Error while performing Inserting Expense Query.');
        }
        console.log(query.sql);
    });
});


app.get('/purchases', function (req, res) {
    connection.query('SELECT * from purchases WHERE dateTime >= CURDATE()', function(err, rows, fields) {
        if (!err) {
            res.send(JSON.stringify(rows));
            console.log('The solution is: ', rows);
        } else {
            console.log('Error while retrieving todays purchases.');
        }
    });
});



app.get('/expenses', function (req, res) {
    connection.query('SELECT * from expenses WHERE dateTime >= CURDATE()', function(err, rows, fields) {
        if (!err) {
            res.send(JSON.stringify(rows));
            console.log('The solution is: ', rows);
        } else {
            console.log('Error while retrieving todays expenses.');
        }
    });
});

app.post('/searchResults', function (req, res) {
    let post = req.body,
        searchConditions = '',
        query,
        dateFrom,
        dateTimeTo,
        results;

    _.each(post, function(inputValue, inputName) {
        if (!_.isUndefined(inputValue) && inputValue !== '' && !/datetime/i.test(inputName)) {
            searchConditions +=  inputName + " LIKE  '%" + inputValue + "%' and ";
        }
    });

    if (!_.isUndefined(post['dateTimeFrom']) && !_.isUndefined(post['dateTimeTo']) &&
        post['dateTimeFrom'] !== '' && post['dateTimeTo'] !== '') {
        dateFrom = post['dateTimeFrom'] + ' 00:00:00';
        dateTimeTo = post['dateTimeTo'] + ' 23:59:59';
        searchConditions += " dateTime BETWEEN '" + dateFrom + "' and '" +  dateTimeTo + "'";
    }

    searchConditions = searchConditions.replace(/and( )?$/, '');

    query = connection.query("\
        SELECT * \
        FROM \
            purchases\
        WHERE " + searchConditions,
        function(err, sales, fields) {
            if (!err) {
                //res.send(JSON.stringify(rows));
                console.log('The sales are: ', sales);

                query = connection.query("\
                SELECT * \
                FROM \
                    expenses\
                WHERE dateTime BETWEEN '" + dateFrom + "' and '" +  dateTimeTo + "'",
                    function(err, expenses, fields) {
                        if (!err) {
                            results = {
                                sales: sales,
                                expenses: expenses
                            };

                            res.send(JSON.stringify(results));
                            console.log('The expenses are: ', expenses);
                        } else {
                            console.log('Error while retrieving search expenses results.');
                        }
                    });
            } else {
                console.log('Error while retrieving search sales results.');
            }
    });

    console.log(query.sql);
});

app.post('/searchResultsPerDay', function (req, res) {
    let post = req.body,
        searchConditions = '',
        dateFrom,
        dateTimeTo,
        query,
        results;

    if (!_.isUndefined(post['dateTimeFrom']) &&
        !_.isUndefined(post['dateTimeTo']) &&
        post['dateTimeFrom'] !== '' &&
        post['dateTimeTo'] !== '') {

        _.each(post, function (inputValue, inputName) {
            if (!_.isUndefined(inputValue) && inputValue !== '' && !/datetime/i.test(inputName)) {
                searchConditions += inputName + " LIKE  '%" + inputValue + "%' and ";
            }
        }.bind(this));

        dateFrom = post['dateTimeFrom'] + ' 00:00:00';
        dateTimeTo = post['dateTimeTo'] + ' 23:59:59';
        searchConditions += " dateTime BETWEEN '" + dateFrom + "' and '" +  dateTimeTo + "'";
        searchConditions = searchConditions.replace(/and( )?$/, '');

        query = connection.query("\
            SELECT \
                sum(supplierPrice) as sumDaySupplierPrice, \
                sum(sellPrice) as sumDayInitialPrice, \
                sum(soldPrice) as sumDayMoneyCollected, \
                sum(moneyCollected) as sumDayRealMoneyCollected, \
                sum(priceMultiplier) as sumPriceMultiplier, \
                COUNT(*) \ as totalRows, \
                DATE(dateTime) as dateTime  \
            FROM \
                purchases\
            WHERE " + searchConditions + "\
            GROUP BY DATE(dateTime)",
            function (err, sales, fields) {
                if (!err) {
                    //res.send(JSON.stringify(sales));
                    console.log('The solution is: ', sales);


                    query = connection.query("\
                        SELECT \
                            sum(price) as price,\
                            DATE(dateTime) as dateTime  \
                        FROM \
                            expenses\
                        WHERE dateTime BETWEEN '" + dateFrom + "' and '" +  dateTimeTo + "'\
                        GROUP BY DATE(dateTime)",
                        function (err, expenses, fields) {
                            if (!err) {
                                results = {
                                    sales: sales,
                                    expenses: expenses
                                };

                                res.send(JSON.stringify(results));
                                console.log('The solution is: ', expenses);
                            } else {
                                console.log('Error while retrieving search expenses results per day.');
                            }
                        });
                } else {
                    console.log('Error while retrieving search results.');
                }
            });

        console.log(query.sql);
    } else {
        console.log('Not specified date from and date to');
    }
});

app.listen(7777, function () {
    console.log('Example app listening on port 7777!')
});
